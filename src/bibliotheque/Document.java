package bibliotheque;

public interface Document {
	void emprunter(Abonne ab) throws PasLibreException;

	int numero();

	void reserver(Abonne ab) throws PasLibreException;

	void retour(); // document rendu ou annulation réservation
}
