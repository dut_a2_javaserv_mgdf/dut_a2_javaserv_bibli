package bibliotheque;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Bibliotheque {

	private String nom;
	private Map<Integer, Document> docs;
	private Map<Integer, Abonne> abonnes;

	public Bibliotheque(String nom, Set<Document> docs, Set<Abonne> abonnes) {
		this.nom = nom;
		this.docs = initDocs(docs);
		this.abonnes = initAbonnes(abonnes);
	}

	// TODO verifier existence abonne
	public void emprunter(int numDoc, int numAb) throws PasLibreException, AbonneInexistant, DocumentInexistant {
		if (!abonnes.containsKey(numDoc))
			throw new DocumentInexistant("Le numero specifie ne correspond � aucun document existant");
		if (!abonnes.containsKey(numAb))
			throw new AbonneInexistant("Le numero specifie ne correspond � aucun abonne existant");
		docs.get(numDoc).emprunter(abonnes.get(numAb));
	}

	public void reserver(int numDoc, int numAb) throws PasLibreException, AbonneInexistant, DocumentInexistant {
		if (!abonnes.containsKey(numDoc))
			throw new DocumentInexistant("Le numero specifie ne correspond � aucun document existant");
		if (!abonnes.containsKey(numAb))
			throw new AbonneInexistant("Le numero specifie ne correspond � aucun abonne existant");
		docs.get(numDoc).reserver(abonnes.get(numAb));
	}

	private Map<Integer, Abonne> initAbonnes(Set<Abonne> ab) {
		HashMap<Integer, Abonne> m = new HashMap<>();
		for (Abonne a : ab)
			m.put(a.numero(), a);
		return m;
	}

	private Map<Integer, Document> initDocs(Set<Document> docs) {
		HashMap<Integer, Document> m = new HashMap<>();
		for (Document d : docs)
			m.put(d.numero(), d);
		return m;
	}

	public String getNom() {
		return nom;
	}
}
