package document;

import java.util.Timer;

import bibliotheque.Abonne;
import bibliotheque.Document;
import bibliotheque.PasLibreException;

public class Livre implements Document {

	private static int cptNum = 0;

	private static Timer timer = new Timer();

	private int numero;
	private String titre;

	private String auteur;

	private Abonne titulaire;

	private boolean reserve;
	private boolean emprunte;


	public Livre(String titre, String auteur) {
		numero = cptNum++;
		titulaire = null;
		this.titre = titre;
		this.auteur = auteur;
		reserve = false;
		emprunte = false;
	}

	@Override
	public void emprunter(Abonne ab) throws PasLibreException {
		if (emprunte) {
			if (titulaire == ab)
				throw new PasLibreException("Vous avez deja emprunte " + titre + " de " + auteur);
			throw new PasLibreException("Le livre " + titre + " de " + auteur + " est deja empunte");
		}
		if (reserve && titulaire != ab)
			throw new PasLibreException("Le livre " + titre + " de " + auteur + " est deja reserve");
		reserve = false;
		emprunte = true;
		titulaire = ab;
	}

	@Override
	public int numero() {
		return numero;
	}

	@Override
	public void reserver(Abonne ab) throws PasLibreException {
		if (emprunte) {
			if (titulaire == ab)
				throw new PasLibreException("Vous avez deja emprunte " + titre + " de " + auteur);
			throw new PasLibreException("Le livre " + titre + " de " + auteur + " est deja empunte");
		}
		if (reserve) {
			if (titulaire == ab)
				throw new PasLibreException("Vous avez deja reserve " + titre + " de " + auteur);
			throw new PasLibreException("Le livre " + titre + " de " + auteur + " est deja reserve");
		}
		reserve = true;
		timer.schedule(new TimerReservation(this), System.currentTimeMillis() + TimerReservation.getDureeRes());
		emprunte = false;
		titulaire = ab;
	}

	@Override
	public void retour() {
		titulaire = null;
		reserve = false;
		emprunte = false;
	}
}
