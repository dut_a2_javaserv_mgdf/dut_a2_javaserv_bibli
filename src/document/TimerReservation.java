package document;

import java.util.TimerTask;

import bibliotheque.Document;

public class TimerReservation extends TimerTask {

	private static final long DUREE_RES = 7200000; // C'est 2h en millisecondes
	private Document doc;

	public TimerReservation(Document d) {
		doc = d;
	}

	@Override
	public void run() {
		doc.retour();
	}

	public static long getDureeRes() {
		return DUREE_RES;
	}

}
