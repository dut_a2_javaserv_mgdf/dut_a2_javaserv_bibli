package abonne;

import bibliotheque.Abonne;

public class AbonneStandard implements Abonne {

	private static int cptNum = 0;
	private int numero;
	private String nom, prenom;

	public AbonneStandard(String nom, String prenom) {
		numero = cptNum++;
		this.nom = nom;
		this.prenom = prenom;
	}

	@Override
	public int numero() {
		return numero;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

}
